module.exports = function(grunt)
{
	grunt.initConfig({
		//Creacion de tareas
		concat: {
			options: 
			{
			separator: ';',
			},
			js: 
			{
				src: ['Programa/original/*'],
				dest: 'ProgramaComprimido/metodosconcat.js',
			},			
		},
		
		uglify:
		{
			options:
			{
				
			},
			my_target:
			{							
				files:
				{
					'ProgramaComprimido/Minificados/metodos.min.js': ['ProgramaComprimido/metodosconcat.js']
				},
			},
		},
		
		copy:{
			main:{
				files:[
					{expand: true, src: ['ProgramaComprimido/**'], dest:'CopyProgramaComprimido'}
				],
			},
		},
		commands:
		{
			 run_sh:{
				 type : 'sh',
				 cmd  : 'chmod -x executesh.sh'
			 },
		},
	});
	//Tareas Personalizadas
	grunt.registerTask('default',['concat']);
	grunt.registerTask('minificar',['uglify']);
	grunt.registerTask('copiar',['copy']);

	//Llamado de las tareas 
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-commands');
}