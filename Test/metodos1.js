var expect = require("chai").expect;
var metodos1 = require("../Programa/metodos1");

describe("Primeros 3 metodos",function()
{	
	describe("Operacion de Fibonacci",function()
	{
		it("realizar operacion",function()
		{
			var pru1 = metodos1.Testfibonnacci(3);
			var pru2 = metodos1.Testfibonnacci(7);
			var pru3 = metodos1.Testfibonnacci(10);
			
			expect(pru1).to.deep.equal(2);
			expect(pru2).to.deep.equal(13);
			expect(pru3).to.deep.equal(55);
		});
	});
	
	describe("Operacion de instructioncase",function()
	{
		it("realizar operacion",function()
		{
			var pru1 = metodos1.Testinstructioncase(1);
			var pru2 = metodos1.Testinstructioncase(2);
			var pru3 = metodos1.Testinstructioncase(3);
			
			expect(pru1).to.deep.equal("caso 1");
			expect(pru2).to.deep.equal("caso 2");
			expect(pru3).to.deep.equal("caso 3");
		});
	});
	
	describe("Operacion de expregular",function()
	{
		it("realizar operacion",function()
		{
			var pru1 = metodos1.Testexpregular("hola");
			var pru2 = metodos1.Testexpregular(2);
			var pru3 = metodos1.Testexpregular("comida");
			
			expect(pru1).to.deep.equal(false);
			expect(pru2).to.deep.equal(true);
			expect(pru3).to.deep.equal(false);
		});
	});
});