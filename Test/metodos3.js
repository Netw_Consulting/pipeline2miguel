var expect = require("chai").expect;
var metodos3 = require("../Programa/metodos3");

describe("Primeros 3 metodos",function()
{
	describe("Operacion de contarword",function()
	{
		it("realizar operacion",function()
		{
		    var pru1 = metodos3.Testcontarword("hola");
			var pru2 = metodos3.Testcontarword("netw");
			var pru3 = metodos3.Testcontarword(10);
			
			expect(pru1).to.deep.equal(4);
			expect(pru2).to.deep.equal(4);
		});
	});
	
	describe("Operacion de formulageneral",function()
	{
		it("realizar operacion",function()
		{
		    var pru1 = metodos3.Testformulageneral(1,3,2);
			var pru2 = metodos3.Testformulageneral(5,6,1);
			
			expect(pru1).to.deep.equal(-1);
			expect(pru2).to.deep.equal(-5);
		});
	});
	
	describe("Operacion de contarmayusandminus",function()
	{
		it("realizar operacion",function()
		{
		    var pru1 = metodos3.Testcontarmayusandminus("HoLa");
			var pru2 = metodos3.Testcontarmayusandminus("FFFF");
			
			expect(pru1).to.deep.equal("mayus= 2 minus= 2");
			expect(pru2).to.deep.equal("mayus= 4 minus= 0");
		});
	});
});