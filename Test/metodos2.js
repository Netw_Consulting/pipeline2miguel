var expect = require("chai").expect;
var metodos1 = require("../Programa/metodos2");

describe("Primeros 3 metodos",function()
{
	describe("Operacion de exregular2",function()
	{
		it("realizar operacion",function()
		{
			var pru1 = metodos1.Testexregular2("hola");
			var pru2 = metodos1.Testexregular2("net w");
			var pru3 = metodos1.Testexregular2(10);
			
			expect(pru1).to.deep.equal(true);
			expect(pru2).to.deep.equal(true);
			expect(pru3).to.deep.equal(false);
		});
	});
	
	describe("Operacion de fecha",function()
	{
		it("realizar operacion",function()
		{
			var pru1 = metodos1.Testfecha("septiembre");
			var pru2 = metodos1.Testfecha("octubre");
			var pru3 = metodos1.Testfecha("noviembre");
			
			expect(pru1).to.deep.equal("error");
			expect(pru2).to.deep.equal("error");
			expect(pru3).to.deep.equal("Estamos a noviembre");
		});
	});
	
	describe("Operacion de sumatoria",function()
	{
		it("realizar operacion",function()
		{
			var pru1 = metodos1.Testsumatoria(2,5);
			var pru2 = metodos1.Testsumatoria(3,7);
			var pru3 = metodos1.Testsumatoria(4,4);
			
			expect(pru1).to.deep.equal(95);
			expect(pru2).to.deep.equal(511);
			expect(pru3).to.deep.equal(79);
		});
	});
});